I. Installer le router
1. Rajouter le Router Module et créer 2-3 routes dedans, ainsi que la wildcard et la redirection
2. Créer les components pour ces routes, faire un ListComponent (avec "list" comme path), un NotFoundComponent (avec la wildcard comme path) et un AboutComponent (avec "about" comme path)

II. Créer un menu générique
1. Créer un nouveau component MenuComponent
2. Dans le dossier de ce component, créer une interface Link qui aura comme propriété label:string et url:string
3. Dans ce component, lui mettre une propriété links:Link[] qui contiendra un tableau des liens qu'on veut afficher
4. Dans le template, faire un ptit nav avec un ul>li dedans et faire un for sur le li sur la propriété links pour l'affichage des liens
5. Modifier la propriété links pour faire que ça soit un input (et l'initialiser comme un tableau vide)
6. Dans le AppComponent, créer une propriété avec nos liens dedans et dans le template, faire appel au menu component en lui donnant les liens dans sa propriété [links]
Bonus : Modifier l'interface link pour rajouter une propriété sub?:Link[] et faire en sorte dans le menu de gérer les sous menus

III. Liste de produits
1. Installer le json-server et ajouter un script dans le package.json pour le lancer comme dans l'autre projet
2. Créer un fichier db.json à la racine et dedans mettre votre base de donnée qui contiendra des produits ressemblant à ça : {"id":1, "label":"Mon Produit", "price":10, "category":"Ma Catégorie"}
Et des category ressemblant à ça : {"id":1, "label":"Ma Catégorie"}
3. Créer une interface entity/category et une interface entity/product dans votre projet (l'entity product aura une category de type string dans ses propriétés)(modifié)
4. Créer un service/product et un service/category
5. Ajouter le HttpClientModule dans votre AppModule
6. Faire le CRUD pour les products (pas besoin de pipe(map) ils ont déjà le format voulu)
7. Créer un TabComponent qui servira à afficher les produits sous forme de table html
8. Faire que le TabComponent ait une propriété  input list:Product[]
9. Dans le template utiliser la propriété list pour faire une boucle et afficher les produit sous forme  de table
10. Dans le ListComponent, rajouter une propriété products et aller chercher la liste des produits via le ProductService (préalablement injecté dans le constructor du ListComponent)
11. Dans le template du ListComponent, mettre un app-tab en lui donnant en [list] les produits venant du ListComponent

IV. Modifier categorie dans le tableau
1. Créer le CR (le add et le findAll) du CategoryService
2. Modifier le template du TabComponent pour faire que la propriété category ne soit pas directement affichée, mais soit à l'intérieur d'un select
(il va falloir mettre un ngModel sur ce select et donc ajouter le FormsModule dans notre AppModule)(modifié)
3. Créer un nouvel input categories dans le TabComponent qui sera typé Category[]
4. Retour dans le template, à l'intérieur du select, faire que les balises options soient générées avec un ngFor en se basant sur la propriété categories du component
5. Dans le ListComponent, injecter le CategoryComponent et faire la même chose qu'avec les products, à savoir, un ptit findAll dont on met le résultat dans une propriété qu'on donne à manger au app-tab
6. Dans le template du TabComponent, mettre un *ngIf sur le select pour dire qu'il ne s'affichera que si la taille du tableau categories est supérieur à zéro
V. Event Update
1. Dans le TabComponent, rajouter une propriété changeCategory et lui mettre un @Output, et lui assigner un EventEmitter<Product> comme type (et une instance de la même chose comme valeur)
2. Dans le TabComponent toujours, créer une méthode onUpdate(product) qui déclenchera le emit de la propriété changeCategory en lui donnant comme paramètre le product
3. Dans le template du TabComponent, rajouter un event (change) sur le select et lui dire de declencher le onUpdate en lui donnant en paramètre le product actuel de la boucle
4. Dans le ListComponent, rajouter une méthode updateProduct(product) qui utilisera le ProductService pour faire un update en lui donnant le product en paramètre
5. Dans le template du ListComponent, sur le app-tab rajouter un event (changeCategory) en lui disant de declencher la méthode updateProduct en lui fournissant $event comme paramètre

VI. Ajouter des produits
1. Créer un nouveau component FormProduct
2. Dans ce component, créer une propriété product:Product et l'initialiser comme un product vierge
3. Créer ensuite une propriété formSubmit et lui mettre dedans une instance de EventEmitter<Product>
4. Créer une méthode onSubmit() qui fera un emit de l'event en lui donnant comme paramètre la prorpiété product du component
5. Dans le template du FormProduct, créer un formulaire avec les inputs requis (en leur mettant bien un name), pour chaque input, on met un ngModel pour l'assigner à une des propriétés du product du component
6. Faire qu'au submit du formulaire, on déclenche la méthode onSubmit du component
7. Dans le ListComponent, rajouter une méthode addProduct(product:Product) qui utilisera le ProductService pour faire un add (faire que dans le subscribe on mette à jour la liste de produit)
8. Dans le template du ListComponent, ajouter un app-form-product et lui dire que au (formSubmit) on déclenche la méthode addProduct en lui donnant $event en argument
9. Bonus (plus ou moins) : Faire que dans le template du FormProduct, la propriété category du product soit liée à un select dont les options sont générées à partir de la base de données, comme dans le TabComponent (il faudra donc rajouter un @Input et lui passer la liste des categories depuis le ListComponent, mais pas besoin du (change) et de l'EventEmitter)

VII. Bonus : Faire qu'on puisse ajouter une nouvelle category via le "select"
(ce sera corrigé sur le gitlab sur la branch bonus dans l'après midi)
1. Dans le template du TabComponent, supprimer le select, mettre un input type text à la place (avec le ngModel dessus quand même)
2. En dessous de cet input, créer une section avec à l'intérieur la liste des catégories (au format ul>li par exemple) générée par le *ngFor
3. Mettre un event au click sur le li qui assignera le label de la category actuelle à la category du product de la boucle puis déclencher la méthode onUpdate en lui donnant le product
4. Sur la section des categories, rajouter un *ngIf pour qu'elle ne s'affiche que si l'input de la category est focus
5. Dans le component, rajouter un nouvel output addCategory et une methode onAdd(product:Product) qui déclenchera cet output en lui donnant le product comme paramètre(modifié)
6. Dans le template, rajouter un event (keyup.enter) sur l'input category et lui faire déclencher la méthode onAdd avec prod comme paramètre(modifié)
7. Dans le ListComponent, rajouter une méthode addCategory(product:Product) qui commencera par faire un add avec le service category en lui donnant comme paramètre {label:product.category} puis lui faire mettre à jour la liste des catégories et lui faire déclencher la méthode updateProduct en lui donnant le product en paramètre