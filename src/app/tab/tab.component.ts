import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../entity/product';
import { Category } from '../entity/category';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {

  @Input()
  data:Product[];

  @Input()
  categories:Category[];

  @Output()
  changeCategory = new EventEmitter<Product>();

  constructor() { }

  ngOnInit() {
  }

  onUpdate(product:Product) {
    
    this.changeCategory.emit(product);
  }



}
