import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { Product } from '../entity/product';
import { Category } from '../entity/category';
import { CategoryService } from '../service/category.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  products:Product[] = [];
  categories:Category[] = [];

  constructor(private prodServ:ProductService, private catServ:CategoryService) { }

  ngOnInit() {
    this.prodServ.findAll().subscribe(prods => this.products = prods);
    this.catServ.findAll().subscribe(cats => this.categories = cats);

  }

  updateProduct(product:Product) {
    this.prodServ.update(product).subscribe();
  }

  addProduct(product:Product) {
    this.prodServ.add(product)
    .subscribe(prod => this.products.push(prod));
  }
  
}
