import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../entity/product';

/**
 * Le service http suivant est très générique (et généricisable),
 * la plupart des entités auront ces méthodes.
 * Deux choses à prendre en comptes ceci dit :
 * - On a ici aucune modification des données entre le serveur et
 * angular. Si jamais notre entité a des valeurs particulières 
 * (genre une date ou une image), il faudra faire des traitements
 * en plus à l'envoie et à la réception
 * - Les types de retour sont ici valides seulement pour le json-server
 * qui renvoie le Product ajouté au post ou au put, mais si le serveur
 * est fait par nous et qu'on ne fait pas les même retour, il faudra
 * changer ces typages également.
 */
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url = 'http://localhost:3000/product/';

  constructor(private http:HttpClient) { }

  findAll():Observable<Product[]> {
    return this.http.get<Product[]>(this.url);
  }

  find(id:number): Observable<Product> {
    return this.http.get<Product>(this.url + id);
  }

  add(product:Product): Observable<Product> {
    return this.http.post<Product>(this.url, product);
  }

  update(product:Product): Observable<Product> {
    return this.http.put<Product>(this.url + product.id, product);
  }

  delete(id:number): Observable<any> {
    return this.http.delete(this.url + id);
  }

}
